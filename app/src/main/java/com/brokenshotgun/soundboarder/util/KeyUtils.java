package com.brokenshotgun.soundboarder.util;

import android.os.Build;
import android.view.KeyEvent;

/**
 * Created by Jason Petterson on 1/25/2015.
 */
public class KeyUtils {
    public static String keyCodeToString(int keycode) {
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            return KeyEvent.keyCodeToString(keycode).replace("KEYCODE_", "");
        }
        else {
            return keycode + "";
        }
    }
}
