package com.brokenshotgun.soundboarder.util;

/**
 * Created by Jason Petterson on 1/25/2015.
 */
public class StringUtils {
    public static String[] snakeCaseToSentenceCase(String... args) {
        String[] sentenceArray = new String[args.length];
        int i=0;
        for(String arg : args) {
            sentenceArray[i] = arg.replace("_", " ").toLowerCase();
            String firstLetter = sentenceArray[i].substring(0, 1);
            sentenceArray[i] = sentenceArray[i].replaceFirst(firstLetter, firstLetter.toUpperCase());
            ++i;
        }
        return sentenceArray;
    }
}
