package com.brokenshotgun.soundboarder.activities;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.brokenshotgun.soundboarder.R;
import com.brokenshotgun.soundboarder.models.SoundCue;
import com.brokenshotgun.soundboarder.util.KeyUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Jason Petterson on 1/11/2015.
 */
@EActivity(R.layout.activity_edit_cue)
public class EditCueActivity extends ActionBarActivity {

    @ViewById(R.id.fileText)
    TextView fileText;

    @ViewById(R.id.assignedKeyText)
    TextView assignedKeyText;

    @ViewById(R.id.volume_bar)
    SeekBar volumeBar;

    @ViewById(R.id.priority_text)
    EditText priorityText;

    @ViewById(R.id.loop_text)
    EditText loopText;

    @ViewById(R.id.rate_bar)
    SeekBar rateBar;

    @Extra
    SoundCue cue;

    @Extra
    int index;

    @AfterViews
    void init() {
        fileText.setText(cue.file.getLastPathSegment());
        assignedKeyText.setText("Assigned Key = " + KeyUtils.keyCodeToString(cue.assignedKey));
        volumeBar.setMax(100);
        volumeBar.setProgress((int) (cue.volume * volumeBar.getMax()));
        priorityText.setText("" + cue.priority);
        loopText.setText(""+cue.loop);
        rateBar.setMax(200);
        rateBar.setProgress((int)(cue.rate * rateBar.getMax()));
    }

    @Click(R.id.save_button)
    void saveButtonClicked() {
        cue.volume = (volumeBar.getProgress() / (float)volumeBar.getMax());
        cue.priority = Integer.parseInt(priorityText.getText().toString());
        cue.loop = Integer.parseInt(loopText.getText().toString());
        cue.rate = rateBar.getProgress() / (rateBar.getMax() / 2.0f);

        Intent result = new Intent();
        result.putExtra("index", index);
        result.putExtra("cue", cue);
        setResult(RESULT_OK, result);
        finish();
    }
}
