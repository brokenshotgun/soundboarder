package com.brokenshotgun.soundboarder.models;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jason Petterson on 1/8/2015.
 */
public class SoundCue implements Parcelable {
    public final Uri file;
    public float volume;
    public int priority;
    public int loop;
    public float rate;
    public int assignedKey;
    public int duration;
    public final List<Integer> triggers;

    public SoundCue(Context context, Uri file) {
        this.file = file;
        this.volume = 1.0f;
        this.priority = 0;
        this.loop = 0;
        this.rate = 1.0f;
        this.assignedKey = -1;
        this.duration = getSoundDuration(context, file);
        triggers = new ArrayList<>();
    }

    private int getSoundDuration(Context context, Uri file){
        MediaPlayer player = MediaPlayer.create(context, file);
        int duration = player.getDuration();
        return duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SoundCue soundCue = (SoundCue) o;

        if (assignedKey != soundCue.assignedKey) return false;
        if (loop != soundCue.loop) return false;
        if (priority != soundCue.priority) return false;
        if (Float.compare(soundCue.rate, rate) != 0) return false;
        if (Float.compare(soundCue.volume, volume) != 0) return false;
        if (!file.equals(soundCue.file)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = file.hashCode();
        result = 31 * result + (volume != +0.0f ? Float.floatToIntBits(volume) : 0);
        result = 31 * result + priority;
        result = 31 * result + loop;
        result = 31 * result + (rate != +0.0f ? Float.floatToIntBits(rate) : 0);
        result = 31 * result + assignedKey;
        result = 31 * result + duration;
        return result;
    }

    @Override
    public String toString() {
        return "SoundCue{" +
                "file=" + file +
                ", volume=" + volume +
                ", priority=" + priority +
                ", loop=" + loop +
                ", rate=" + rate +
                ", assignedKey=" + assignedKey +
                ", duration=" + duration +
                ", triggers=" + triggers.toString() +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(file.toString());
        out.writeFloat(volume);
        out.writeInt(priority);
        out.writeInt(loop);
        out.writeFloat(rate);
        out.writeInt(assignedKey);
        out.writeInt(duration);
        out.writeList(triggers);
    }

    public static final Parcelable.Creator<SoundCue> CREATOR = new Parcelable.Creator<SoundCue>() {
        public SoundCue createFromParcel(Parcel in) {
            return new SoundCue(in);
        }

        public SoundCue[] newArray(int size) {
            return new SoundCue[size];
        }
    };

    private SoundCue(Parcel in) {
        file = Uri.parse(in.readString());
        volume = in.readFloat();
        priority = in.readInt();
        loop = in.readInt();
        rate = in.readFloat();
        assignedKey = in.readInt();
        duration = in.readInt();
        triggers = in.readArrayList(Integer.class.getClassLoader());
    }
}
